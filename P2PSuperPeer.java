import java.net.*;
import java.io.*;

public class P2PSuperPeer {
    public static void main(String[] args) throws IOException {
        Socket clientSocket = null; // socket for the thread
        Object [][] RoutingTable = new Object [10][2]; // routing table
        int SockNum = 5555; // port number
        Boolean Running = true;
        int ind = 0; // indext in the routing table

        //Accepting connectionsC
        ServerSocket serverSocket = null; // server socket for accepting connections
        try {
            serverSocket = new ServerSocket(5555);
            System.out.println("Super Peer is Listening on port: 5555.");
        }
        catch (IOException e) {
            System.err.println("Could not listen on port: 5555.");
            System.exit(1);
        }

        // Creating threads with accepted connections
        while (Running)
        {
            try {
                clientSocket = serverSocket.accept();
                (new P2PSThread(RoutingTable, clientSocket, ind)).start(); // creates a thread with a random port
                ind++; // increments the index

                ind = ind % 10;//overwrites oldest index if over 10 connections

                System.out.println("Super Peer connected with Peer: " + clientSocket.getInetAddress().getHostAddress());
            }
            catch (IOException e) {
                System.err.println("Peer failed to connect.");
                System.exit(1);
            }
        }//end while

        //closing connections
        clientSocket.close();
        serverSocket.close();

    }
}