import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

public class commPackage{
    Socket socket;
    PrintWriter out;
    BufferedReader in;
    public commPackage(Socket newSocket, PrintWriter newOut, BufferedReader newIn){
        socket = newSocket;
        out = newOut;
        in = newIn;
    }
    public commPackage(){
        socket = null;
        out = null;
        in = null;
    }
    public Socket getSocket(){
        return socket;
    }
    public void setSocket(Socket newSocket){
        socket = newSocket;
    }
    public PrintWriter getOut(){
        return out;
    }
    public void setOut(PrintWriter newOut){
        out = newOut;
    }

    public BufferedReader getIn() {
        return in;
    }
    public void setIn(BufferedReader newIn){
        in = newIn;
    }
}