import java.io.*;
import java.net.*;
import java.lang.Exception;


public class P2PSThread extends Thread
{
    private Object [][] RTable; // routing table
    private PrintWriter out, outTo; // writers (for writing back to the machine and to destination)
    private BufferedReader in; // reader (for reading from the machine connected to)
    private String inputLine, outputLine, destination, addr, origin; // communication strings
    private Socket outSocket; // socket for communicating with a destination
    private int ind; // indext in the routing table
    private int SockNum = 5555;
    private String otherRouterName = "192.168.1.67"; //the name/location of the other SUPER router.
    private commPackage cm;
    private PrintWriter toSuper;
    private BufferedReader fromSuper;
    private String msg;
    private Boolean notPeer;
    private Socket socket;

    // Constructor
    P2PSThread(Object [][] Table, Socket toClient, int index) throws IOException
    {
        out = new PrintWriter(toClient.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(toClient.getInputStream()));
        RTable = Table;
        addr = toClient.getInetAddress().getHostAddress();
        RTable[index][0] = addr; // IP addresses
        RTable[index][1] = toClient; // sockets for communication
        ind = index;
        socket = null;
    }

    // Run method (will run for each machine that connects to the ServerRouter)
    public void run()
    {
        try
        {
            // Initial sends/receives
            destination = in.readLine(); // initial read (the destination for writing)
            if(destination.equals("SUPER")){
                destination = in.readLine();
                notPeer = true;
                System.out.println("Talking to fellow SUPER");
                System.out.println("Checking if there is " + destination);
                System.out.println("Retrieving origin...");
                origin = in.readLine();
            }else {
                notPeer = false;
                System.out.println("Talking to lowly Peer");
                System.out.println("Checking if there is " + destination);
                out.println("Connected to the Super Peer."); // confirmation of connection
            }

            // waits 5 seconds to let the routing table fill with all machines' information
            try{
                Thread.currentThread().sleep(5000);
            }
            catch(InterruptedException ie){
                System.out.println("Thread interrupted");
            }

            // loops through the routing table to find the destination
            if(notPeer){
                Boolean destinationFound = false;
                System.out.println("Searching through routing table...");
                for (int i = 0; i < 10; i++) {
                    if (destination.equals((String) RTable[i][0])) {
                        outSocket = (Socket) RTable[i][1]; // gets the socket for communication from the table
                        System.out.println("Found destination: " + destination);
                        outTo = new PrintWriter(outSocket.getOutputStream(), true); // assigns a writer
                        System.out.println("Telling Peer to connect...");
                        outTo.println("CONNECT");
                        outTo.println(origin);
                        destinationFound = true;
                        out.println("FOUND");
                        break;
                    }
                }
                if(destinationFound == false) {
                    System.out.println(destination+ "is not found in the P2P network.");
                    out.println("NOTFOUND");

                }
            }else {
                Boolean destinationFound = false;
                System.out.println("Searching through routing table...");
                for (int i = 0; i < 10; i++) {
                    if (destination.equals((String) RTable[i][0])) {
                        outSocket = (Socket) RTable[i][1]; // gets the socket for communication from the table
                        System.out.println("Found destination: " + destination);
                        outTo = new PrintWriter(outSocket.getOutputStream(), true); // assigns a writer
                        destinationFound = true;
                        System.out.println("Telling Peers to connect...");
                        outTo.println("CONNECT");
                        outTo.println(origin);
                        out.println("CONNECT");
                        out.println(destination);
                        break;
                    }
                }
                if (destinationFound == false) {
                    System.out.println("Couldn't find "+ destination + " checking with other Super Peer...");
                    socket = new Socket(otherRouterName, SockNum);
                    toSuper =  new PrintWriter(socket.getOutputStream(), true);
                    fromSuper = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    System.out.println("Connected to the other Super Peer...");
                    toSuper.println("SUPER");
                    toSuper.println(destination);
                    toSuper.println(addr);
                    msg = fromSuper.readLine();
                    if (msg.equals("FOUND")) {
                        System.out.println(destination+" is located with the other Super Peer.");
                        out.println("CONNECT");
                        out.println(destination);
                    } else {
                        System.out.println(destination+ "is not found in the P2P network.");
                        out.println("NOTFOUND");
                    }
                }
            }

        }// end try
        catch (IOException e) {
            System.err.println("Could not listen to socket.");
            System.exit(1);
        }
    }
    public static commPackage connect(String routerName, int SockNum, commPackage comm) {
        Socket Socket = null; // socket to connect with ServerRouter
        PrintWriter out = null; // for writing to ServerRouter
        BufferedReader in = null; // for reading form ServerRouter
        try {
            Socket = new Socket(routerName, SockNum);
            out = new PrintWriter(Socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(Socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about router: " + routerName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: " + routerName);
            System.exit(1);
        }
        comm.setSocket(Socket);
        comm.setIn(in);
        comm.setOut(out);
        return comm;
    }
}