import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.*;
import java.net.*;
import java.nio.Buffer;
import java.util.Scanner;

public class P2PPeer {
    public static void main(String[] args) throws IOException {
        /*==========================CODE FOR CONNECTING TO A PEER=====================================================*/
        // Variables for setting up connection and communication
        Socket Socket = null; // socket to connect with ServerRouter
        PrintWriter out = null; // for writing to ServerRouter
        BufferedReader in = null; // for reading form ServerRouter
        BufferedReader ti = new BufferedReader((new InputStreamReader(System.in)));
        InetAddress addr = InetAddress.getLocalHost();
        String host = addr.getHostAddress(); // Client machine's IP
        String routerName = "10.80.22.159"; // ServerRouter host name
        int SockNum = 5555; // port number
        int PeerNum = 5554; //port number for peers
        Boolean timeToExit = false;
        String userInput;
        ServerSocket st = null;
        long chars, avgchars;

        while (!timeToExit) {
            // Tries to connect to the ServerRouter
            commPackage comm = new commPackage();
            comm = connect(routerName, SockNum, comm);
            Socket = comm.getSocket();
            out = comm.getOut();
            in = comm.getIn();
            // Variables for message passing
            Reader reader = new FileReader("file.txt");
            BufferedReader fromFile = new BufferedReader(reader); // reader for the string file
            String fromServer; // messages received from ServerRouter
            String fromUser; // messages sent to ServerRouter
            String address;// = "127.0.0.1"; // destination IP (Server)
            long t0, t1, t, avgTime, msgCount, p1, p2;
            System.out.println("Wait for a peer to connect or Connect to a peer?");
            System.out.println("W or C?");
            Boolean validInput = false;
            userInput = ti.readLine();
            while (!validInput) {
                if (userInput.equals("W") || userInput.equals("w")) {
                    validInput = true;
                    System.out.println("Waiting for a peer to connect...");
                    fromServer = in.readLine();
                    if (fromServer.equals("CONNECT")) {
                        try {
                            st = new ServerSocket(PeerNum);
                            System.out.println("Listening on port 5554");
                        } catch (IOException e) {
                            System.err.println("Could not listen on port: 5554.");
                            System.exit(1);
                        }

                        Socket = st.accept();
                        in = new BufferedReader(new InputStreamReader(Socket.getInputStream()));
                        out = new PrintWriter(Socket.getOutputStream(), true);
                        fromServer = in.readLine();
                        if (fromServer.equals(host)) {
                            out.println("BEGIN");
                            //TODO more stuff?
                            while ((fromServer = in.readLine()) != null) {
                                System.out.println("Peer said: " + fromServer);
                                if (fromServer.equals("FINISHED")) { // exit statement
                                    out.println("FINISHED");
                                    break;
                                }
                                System.out.println("I said: RECEIVED");
                                if (out != null)
                                    out.println("RECEIVED"); // sending the converted message back to the Client via ServerRouter
                            }
                        }
                    }
                } else if (userInput.equals("C") || userInput.equals("c")) {
                    p1 = System.currentTimeMillis();
                    validInput = true;
                    System.out.println("Enter the IP address of the peer you would like to connect to.");
                    address = ti.readLine();
                    System.out.println("Looking for " + address + " on assigned super peer.");
                    // Communication process (initial sends/receives
                    out.println(address);// initial send (IP of the destination Server)
                    fromServer = in.readLine();//initial receive from router (verification of connection)
                    while (!fromServer.equals("BEGIN")) {
                        System.out.println("PeerRouter: " + fromServer);
                        fromServer = in.readLine();//after router table lookup server sends a CONNECT message followed by the ip of the destination
                        // or NOTFOUND message and peer state reverts.
                        if (fromServer.equals("CONNECT")) {
                            fromServer = in.readLine();//Super PeerC sends address of destination peer.
                            comm = connect(fromServer, PeerNum, comm);
                            Socket = comm.getSocket();
                            out = comm.getOut();
                            in = comm.getIn();
                            out.println(address);// initial send (IP of the destination peer)
                            fromServer = in.readLine();//initial receive from router (verification of connection)
                        } else if (fromServer.equals("NOTFOUND")) {
                            continue;
                        }
                    }
                    out.println(host); // Client sends the IP of its machine as initial send

                    msgCount = 0;
                    avgTime =0;
                    t0 = System.currentTimeMillis();
                    p2 = System.currentTimeMillis();
                    p1 = p2 - p1;
                    System.out.println("TIME: "+ p1);
                    avgchars =0;
                    // Communication while loop
                    while ((fromServer = in.readLine()) != null) {
                        System.out.println("Peer said: " + fromServer);
                        t1 = System.currentTimeMillis();
                        if (fromServer.equals("FINISHED")) // exit statement
                            break;
                        t = t1 - t0;
                        System.out.println("Cycle time: " + t);
                        avgTime += t;
                        fromUser = fromFile.readLine(); // reading strings from a file
                        if (fromUser != null) {
                            chars = fromUser.length();
                            avgchars += chars;
                            System.out.println("I said: " + fromUser);
                            t0 = System.currentTimeMillis();
                            msgCount++;
                            out.println(fromUser); // sending the strings to the Server via ServerRouter

                        }
                    }
                    avgTime = avgTime/msgCount;
                    avgchars = avgchars/msgCount;
                    System.out.println("AVERAGE CYCLE TIME: "+ avgTime);
                    System.out.println("AVERAGE MSG LENGTH: "+ avgchars);
                    System.out.println("TIME: "+ p1);
                }
            }

                // closing connections
                out.close();
                in.close();
                Socket.close();

                System.out.println("Continue operations?");
                fromUser = ti.readLine();
                if (fromUser.equals("yes") || fromUser.equals("y") || fromUser.equals("Yes") || fromUser.equals("YES"))
                    continue;
                else timeToExit = true;
            }

        }

    public static commPackage connect(String routerName, int SockNum, commPackage comm) {
        Socket Socket = null; // socket to connect with ServerRouter
        PrintWriter out = null; // for writing to ServerRouter
        BufferedReader in = null; // for reading form ServerRouter
        try {
            Socket = new Socket(routerName, SockNum);
            out = new PrintWriter(Socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(Socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about router: " + routerName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: " + routerName);
            System.exit(1);
        }
        comm.setSocket(Socket);
        comm.setIn(in);
        comm.setOut(out);
        return comm;
    }
}
